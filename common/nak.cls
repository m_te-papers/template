%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dokumentenklasse für Transferleistungen
% erstellt am 01.12.2022
% Version: v1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{common/nak}[2022/12/01 v1, by Jens Muenker]

\newif\iflnienglish\lnienglishfalse
\DeclareOption{english}{\lnienglishtrue}

\ProcessOptions

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Seiteneinstellungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\LoadClass[a4paper,oneside,12pt, enabledeprecatedfontcommands, bibliography=totocnumbered]{scrartcl} % Schriftgröße 12pt
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\usepackage[toc,page]{appendix}

\RequirePackage{textalpha} % Griechische Schriftzeichen
\RequirePackage{lmodern} % Gerenderte Schrift
\RequirePackage{eurosym} % Euro Symbol
\addtokomafont{disposition}{\normalfont\bfseries} % Nutze dieselbe Schriftart für Überschriften wie für Text

\RequirePackage{subcaption}

\RequirePackage{setspace}
\setstretch{1.2} %Zeilenabstrand 1.2

\RequirePackage[a4paper, margin=2.5cm, includefoot, heightrounded]{geometry} % Seitenrand 2.5cm
\RequirePackage{microtype}

\RequirePackage[table]{xcolor} % Farben in Tabellen

% Sprache einstellen (Schriftzeichen)
\RequirePackage[english,ngerman,german]{babel}
\iflnienglish\selectlanguage{english}\def\bbl@main@language{english}\fi

% Punkt am Ende der Überschriften-Nummerierung entfernen
\renewcommand{\autodot}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Zitieren mit biber & biblatex
% !BIB TS-program = biber
\usepackage{csquotes, url}
\usepackage[backend=bibtex, citestyle=verbose-ibid, urldate=short, style=ieee]{biblatex} % APA-Zitierstil
\renewcommand*{\bibfont}{\footnotesize} % Adjust the bibliography font size
\addbibresource{quellen}

% Webeseiten-Url-Config
\PassOptionsToPackage{hyphens}{url}
\RequirePackage[hyperfootnotes=false,hidelinks]{hyperref}

% Tikz-Import
\RequirePackage{tikz}
\usetikzlibrary{fadings}

% Für Matheformeln etc.
\RequirePackage{mathptmx}

%Graphicx
\RequirePackage{graphicx}

% Bildunterschriften
\RequirePackage{caption}
\RequirePackage{subcaption}

% Listen
\RequirePackage{listings}

% Für Farben
\RequirePackage{color}

% Mehrfach Footnote
\RequirePackage[symbol]{footmisc}

% Auf Kapitel im Text verweisen
\usepackage{cleveref}

% Abkürzungen
\RequirePackage{acronym}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tabelleneinstellungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{array} % m und b tabellenausrichtungen
\RequirePackage{longtable} % Tabellen über mehrere Spalten
\RequirePackage{booktabs} % Bessere Tabellen
\RequirePackage{multirow} % Tabellen mit multirow Zellen
\renewcommand{\arraystretch}{1.5} % Tabellen: Zeilenabstand
\setlength{\tabcolsep}{12pt} % Tabellen: Spaltenabstand
\usepackage{threeparttable} % Für Tabellen mit Fußnoten

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Figuren im Text
\RequirePackage{wrapfig}

% PDFs im Text verwenden/importieren können
\RequirePackage{pdfpages}
\RequirePackage[export]{adjustbox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nordakademie Header für die Seiten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{fancyhdr}
\addtolength{\headheight}{0.9cm} % Headerhöhe
\pagestyle{fancyplain}
\renewcommand{\sectionmark}[1]{\markboth{#1}{}} 
\fancyhead[R]{\includegraphics[height=0.8cm]{common/Nordakademie_Logo.png}} % Logo rechts
\fancyhead[L]{\itshape\nouppercase{\leftmark}}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[all]{nowidow} % Keine Waisen (einsame Zeile eines Absatzes auf der nächsten Seite)

% Pagestyles
\RequirePackage{floatpag}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Hilfreiche Befehle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\needCite}[1][]{
    \@latex@warning{Still require cite: #1! Needs to be done}
    \colorbox{yellow}{\textcolor{red}{Zitat Notwendig #1}}
}

\newcommand{\todo}[1]{
    \colorbox{yellow}{\textcolor{red}{TODO: #1}}
    \@latex@warning{TODO: #1! Needs to be done}
}

\newcommand{\anf}[1]{\glqq #1\grqq{}}
\newcommand{\anfo}[1]{\glqq #1\grqq}

\newcommand{\fullref}[1]{\ref{#1} \anf{\nameref{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Verbesserte Fussnote
\renewcommand\@makefntext[1]{\leftskip=0.5em\hskip-0.45em\@makefnmark#1}
\renewcommand{\thefootnote}{\arabic{footnote}}

\RequirePackage{float}

% Auskommentieren von LaTex-Elementen und längeren Text-Blöcken
\usepackage{verbatim} 

\def\initminted{
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Programmcode verwenden
    %
    % WICHTIG:
    %
    % Python 3 & das Python-Package
    % python-pygments muss installiert sein!!
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \usepackage{minted}
    \usepackage{xcolor}

    %Konfiguration der Codestyles

    \setminted[js]{ 
        linenos=true,             % Line numbers
        autogobble=true,          % Automatically remove common white space
        frame=lines,
        framesep=2mm,
        fontsize=\footnotesize
    }

    \setminted[abap]{ 
        linenos=true,             % Line numbers
        autogobble=true,          % Automatically remove common white space
        frame=lines,
        framesep=2mm,
        fontsize=\footnotesize
    }

    \setminted[xml]{ 
        linenos=true,             % Line numbers
        autogobble=true,          % Automatically remove common white space
        frame=lines,
        framesep=2mm,
        fontsize=\footnotesize
    }

    \setminted[json]{ 
        linenos=true,             % Line numbers
        autogobble=true,          % Automatically remove common white space
        frame=lines,
        framesep=2mm,
        fontsize=\footnotesize
    }

    \setminted[java]{ 
        linenos=true,             % Line numbers
        autogobble=true,          % Automatically remove common white space
        frame=lines,
        framesep=2mm,
        fontsize=\footnotesize
    }

    \usemintedstyle{lovelace}
    \usemintedstyle[abap]{abap}

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verzeichniseinstellungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand*{\listingautorefname}{\listingscaption}

% Inhaltsverzeichnis
\newcommand{\inhaltsverzeichnis}{
	\newpage
	\hypertarget{toc}
	\phantomsection
    \tableofcontents
}

% Abbildungsverzeichnis
\newcommand{\abbildungsverzeichnis}{
	\newpage
	\phantomsection
	\addcontentsline{toc}{section}{\footnotesize{Abbildungsverzeichnis}}
	\listoffigures
}

% Tabellenverzeichnis
\newcommand{\tabellenverzeichnis}{
	\phantomsection
	\addcontentsline{toc}{section}{\footnotesize{Tabellenverzeichnis}}
	\listoftables
}

% Abkürzungsverzeichnis
\newcommand{\abkuerzungsverzeichnis}{
	\phantomsection
	\section*{Abkürzungsverzeichnis}
	\addcontentsline{toc}{section}{\footnotesize{Abkürzungsverzeichnis}}
        \markboth{Abkürzungsverzeichnis}{Abkürzungsverzeichnis}
	\begin{center}
		\input{abkuerzungen.tex}
	\end{center}
}

% Quellenverzeichnis
\newcommand{\quellenverzeichnis}{
    \newpage	
    \phantomsection
    \section*{Quellenverzeichnis}
    \addcontentsline{toc}{section}{\footnotesize{Quellenverzeichnis}}
    \markboth{Quellenverzeichnis}{Quellenverzeichnis}
    \printbibliography[heading=none]
}

\newcommand{\anhang}{
    \renewcommand\appendixtocname{Anhang}

    \newpage
    \phantomsection
    \section*{Anhang}
    \addcontentsline{toc}{section}{\footnotesize{Anhang}}
    \markboth{Anhang}{Anhang}

    \appendixpageoff
    \appendixtocoff
    \begin{appendices}
        \renewcommand{\thesubsection}{\Alph{subsection}}
        \addtocontents{toc}{\protect\setcounter{tocdepth}{3}}
        \include{appendix/appendix}
    \end{appendices}
}

\newcommand{\sperrvermerk}[1]{
    \phantomsection
    \section*{Sperrvermerk}
    %\addcontentsline{toc}{section}{\footnotesize{Sperrvermerk}}
    Die vorliegende Arbeit mit dem Titel \textbf{\enquote{\hausarbeitsTitel}} beinhaltet interne und vertrauliche Informationen der \textbf{#1}.
    Aus diesem Grund ist eine Einsicht in diese Arbeit nicht gestattet.
    Ausgenommen hiervon sind die betreuenden Dozierenden sowie die befugten Mitglieder des Prüfungsausschusses.
}

\newcommand{\sperrvermerkfreigabe}[1]{
    \\
    \indent Ich bin damit einverstanden, dass diese Hochschulschrift nach Ablauf von \textbf{#1 Jahren} ab Datum der Abgabe
    ohne Rückfrage für die Öffentlichkeit freigegeben wird. 
}

\newcommand{\eidesstattlicheversicherung}{
    \phantomsection
    \section*{Eidesstattliche Versicherung}
    %\addcontentsline{toc}{section}{\footnotesize{Eidesstattliche Versicherung}}
    Hiermit erkläre ich, dass ich die vorliegende Arbeit selbstständig verfasst habe, dass ich sie zuvor an keiner anderen Hochschule und in keinem anderen Studiengang
    als Prüfungsleistung eingereicht habe und dass ich keine anderen als die angegebenen Quellen und Hilfsmittel benutzt habe.

    Alle Stellen der Arbeit, die wörtlich oder sinngemäß aus Veröffentlichungen oder aus anderweitigen fremden Äußerungen entnommen wurden, sind als solche kenntlich gemacht. 
}

\newcommand{\genderdisclaimer}{
    
    \phantomsection
    \section*{Hinweis zur genderneutralen Sprache}
    %\addcontentsline{toc}{section}{\footnotesize{Hinweis zur genderneutralen Sprache}}
    In der vorliegenden Arbeit wird darauf verzichtet, bei Personenbezeichnungen sowohl die weibliche als auch die männliche und diverse Form zu nennen.
    Das generische Maskulinum adressiert alle Leserinnen und Leser und gilt in allen Fällen, in denen dies nicht explizit ausgeschlossen wird, für alle Geschlechter.
}

\newcommand{\unterschrift}[3]{
    \\ 
    #2, den \today
    \hspace*{\fill} 
    \underbar{
        \raisebox{-0.2cm}{
            \vspace*{5mm}
            \includegraphics[width=6cm]{#3}
            \vspace*{5mm}
        }
    }
    
    \hspace*{\fill}
    #1
}



\RequirePackage{comment}

\newcommand{\mailto}[1]{
    \href{mailto:#1}{#1}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
